# Couleuvres et Escabeaux

Le projet est divisé en 4 parties: l'écran de jeu, l'écran de menu, l'écran des
options et l'écran des statistiques.

## Cours du 22 avril 2015

### Changements apportés
* L'écran du menu est intégrée dans l'écran principal
* Les options on été réduites, on se concentre sur la grille de jeu pour tout de suite
* La branche `master` est verrouillée, on doit faire les changements sur d'autres branches et faire un "pull request"

## Écran de jeu

L'écran de jeu doit lire un fichier de configuration écrit par l'écran de menu
et écrire un fichier qui pourra être lu par l'écran des statistiques.

## Écran de menu

L'écran de menu sert à choisir les différents écrans disponibles: options, jeu
et statistiques.

## Écran des options

L'écran des options sert à configurer le jeu, changer le volume, activer la
musique, etc.
Elle écrit les diverses options dans un fichier qui sera lu par le jeu.

## Écran des statistiques

L'écran des statistiques sert à afficher des statistiques du jeu, comme le
nombre de parties jouées, le nombre de cases gagnées ou perdues, etc.
