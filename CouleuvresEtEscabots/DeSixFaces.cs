﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace CouleuvresEtEscabeaux
{
    class DeSixFaces
    {
        public Button BoutonDe = new Button();
        public int taille = 120;
        public int dernierResultat = 1;            // Dernier résultat obtenu
        private int borneInferieure = 1;
        private int borneSuperieure = 6;
        private Random aleatoire = new Random();    // Générateur de nombres aléatoires

        public DeSixFaces()
        {
            this.configurerBoutonDe();
            this.activerBoutonDe();
        }

        public void lancer()
        {
            this.desactiverBoutonDe();

            for (int i = 0; i < 20; ++i)
            {
                this.BoutonDe.Refresh();
                this.BoutonDe.BackgroundImage = determinerImageDeFond(i % 6 + 1);
                Thread.Sleep(100);
            }

            this.dernierResultat = this.aleatoire.Next() % (borneSuperieure - borneInferieure + 1) + borneInferieure;
            this.BoutonDe.BackgroundImage = determinerImageDeFond(this.dernierResultat);
        }

        public void activerBoutonDe()
        {
            this.BoutonDe.Enabled = true;
        }

        public void desactiverBoutonDe()
        {
            this.BoutonDe.Enabled = false;
        }

        private void configurerBoutonDe()
        {
            this.BoutonDe.Name = "DeSixFaces";
            this.BoutonDe.Height = this.taille;
            this.BoutonDe.Width = this.taille;
            this.BoutonDe.Top = 60;
            this.BoutonDe.Left = 720;
            this.BoutonDe.BackgroundImage = determinerImageDeFond(dernierResultat);
            this.BoutonDe.BackgroundImageLayout = ImageLayout.Stretch;
            this.BoutonDe.FlatStyle = FlatStyle.Flat;
        }

        private Bitmap determinerImageDeFond(int resultat)
        {
            return (Bitmap)Properties.Resources.ResourceManager.GetObject("Dé_" + resultat);
        }
    }
}
