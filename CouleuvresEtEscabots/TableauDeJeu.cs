﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace CouleuvresEtEscabeaux
{
    class TableauDeJeu
    {

        public Form Fenetre;
        private int nombreDeJoueurs = 4;
        private int nombreDeCasesDeJeu = 100;
        public int Rotation = 0;

        public List<CaseDeJeu> listeDesCasesDeJeu = new List<CaseDeJeu>();
        public List<Joueur> listeDesJoueurs = new List<Joueur>();
        public List<Label> EtiquettesJoueurs = new List<Label>();
        public DeSixFaces deSixFaces = new DeSixFaces();
        private CaseDeJeu caseDeJeuActive;

        Joueur j = new Joueur(0);
        public Joueur[] tabJ = new Joueur[4];

        private Joueur joueurActif;

        public TableauDeJeu(Form form)
        {
            Fenetre = form;
            this.initialiserListeDesCasesDeJeu();
            this.initialiserListeDesJoueurs();
            CreerEtiquettesJoueurs();
            this.determinerJoueurActif();
            this.deSixFaces.BoutonDe.Click += new EventHandler(deSixFaces_Click);
            ChangementEtiquetteJoueurs();
        }

        public void initialisertabJ()
        {
            tabJ[0] = new Joueur(0);
            tabJ[1] = new Joueur(0);
            tabJ[2] = new Joueur(0);
            tabJ[3] = new Joueur(0);
        }

        private void initialiserListeDesCasesDeJeu()
        {
            for (int i = 0; i < nombreDeCasesDeJeu; i++)
            {
                CaseDeJeu caseDeJeu = new CaseDeJeu(i, 60, nombreDeCasesDeJeu);
                listeDesCasesDeJeu.Add(caseDeJeu);
            }
        }

        private void initialiserListeDesJoueurs()
        {
            for (int i = 0; i < nombreDeJoueurs; ++i)
            {
                Point position = listeDesCasesDeJeu[nombreDeCasesDeJeu - 1].obtenirPosition();
                Joueur joueur = new Joueur(i);
                joueur.deplacer(position);
                listeDesJoueurs.Add(joueur);
            }
        }

        private void passerAuProchainJoueur()
        {
            
            if (Rotation < this.nombreDeJoueurs - 1) {
                Rotation++;
            }
            else {
                Rotation = 0;
            }

            ChangementEtiquetteJoueurs();
        }

        private void CreerEtiquettesJoueurs()
        {
            Panel Panneau = new Panel();

            Panneau.Left = 680;
            Panneau.Top = 320;
            Panneau.Width = 200;
            Panneau.Height = 200;
            Panneau.BackgroundImage = Properties.Resources.eau;
            Panneau.BackColor = Color.Red;
            Fenetre.Controls.Add(Panneau);

            for (int i = 0; i < nombreDeJoueurs; ++i)
            {
                Label Etiquette = new Label();

                Etiquette.Text = "Joueur" + (i + 1);
                Etiquette.Left = 20;
                Etiquette.Top = 20 + i * 40;
                Etiquette.Font = new Font("Comic Sans MS", 12);
                Etiquette.ForeColor = ChoisirCouleurSelon(i);
                Etiquette.BackColor = Color.Transparent;
                
                EtiquettesJoueurs.Add(Etiquette);
                Panneau.Controls.Add(Etiquette);
                Etiquette.BringToFront();
            }         
        }

        private Color ChoisirCouleurSelon(int index)
        {
            switch (index)
            {
                case 0:
                    return Color.Red;
                case 1:
                    return Color.Blue;
                case 2:
                    return Color.Goldenrod;
                case 3:
                    return Color.Green;
                default:
                    return Color.Black;
            }
        }

        private void ChangementEtiquetteJoueurs()
        {
            switch (Rotation)
            {
                case 1:
                    EtiquettesJoueurs[0].BackColor = Color.Transparent;
                    EtiquettesJoueurs[0].ForeColor = Color.Red;
                    EtiquettesJoueurs[1].BackColor = Color.Blue;
                    EtiquettesJoueurs[1].ForeColor = Color.Black;
                    break;
                case 2:
                    EtiquettesJoueurs[1].BackColor = Color.Transparent;
                    EtiquettesJoueurs[1].ForeColor = Color.Blue;
                    EtiquettesJoueurs[2].BackColor = Color.Yellow;
                    EtiquettesJoueurs[2].ForeColor = Color.Black;
                    break;
                case 3:
                    EtiquettesJoueurs[2].BackColor = Color.Transparent;
                    EtiquettesJoueurs[2].ForeColor = Color.Yellow;
                    EtiquettesJoueurs[3].BackColor = Color.Green;
                    EtiquettesJoueurs[3].ForeColor = Color.Black;
                    break;
                default:
                    EtiquettesJoueurs[3].BackColor = Color.Transparent;
                    EtiquettesJoueurs[3].ForeColor = Color.Green;
                    EtiquettesJoueurs[0].BackColor = Color.Red;
                    EtiquettesJoueurs[0].ForeColor = Color.Black;
                    break;
            }
        }

        private void determinerCaseActive()
        {
            int CaseTheorique = joueurActif.noCase + deSixFaces.dernierResultat;
            j.casesGagnees += deSixFaces.dernierResultat;
            joueurActif.noCase = CaseTheorique;

            if (joueurActif.EstTropLoin(nombreDeCasesDeJeu))
            {
                int Depassement = joueurActif.noCase - (nombreDeCasesDeJeu);
                j.casesPerdues += Depassement;
                joueurActif.noCase -= (Depassement + 1) * 2;
                j.casesGagnees -= Depassement;
            }
            caseDeJeuActive = listeDesCasesDeJeu[nombreDeCasesDeJeu - joueurActif.noCase - 1];
        }

        private void determinerJoueurActif()
        {
            this.joueurActif = this.listeDesJoueurs[Rotation];
        }

        private void deSixFaces_Click(object sender, EventArgs e)
        {
            this.deSixFaces.lancer();
            this.determinerJoueurActif();
            this.determinerCaseActive();
            this.caseDeJeuActive.activer();
            this.caseDeJeuActive.laCase.Click += new EventHandler(caseDeJeu_Click);
        }

        private void caseDeJeu_Click(object sender, EventArgs e)
        {
            int x = caseDeJeuActive.laCase.Top;
            int y = caseDeJeuActive.laCase.Left;
            Point position = new Point(x, y);

            caseDeJeuActive.desactiver();
            caseDeJeuActive.laCase.Click -= new EventHandler(caseDeJeu_Click);
            deSixFaces.activerBoutonDe();
            joueurActif.deplacer(position);

            if (DeterminerCasesEscabeauxSelon(joueurActif.noCase))
            {
                caseDeJeuActive = listeDesCasesDeJeu[nombreDeCasesDeJeu - joueurActif.noCase - 1];
                x = caseDeJeuActive.laCase.Top;
                y = caseDeJeuActive.laCase.Left;
                position = new Point(x, y);
                this.joueurActif.deplacer(position);
            }

            if (DeterminerCasesCouleuvresSelon(joueurActif.noCase))
            {
                caseDeJeuActive = listeDesCasesDeJeu[nombreDeCasesDeJeu - joueurActif.noCase - 1];
                x = caseDeJeuActive.laCase.Top;
                y = caseDeJeuActive.laCase.Left;
                position = new Point(x, y);
                this.joueurActif.deplacer(position);
            }

            if (joueurActif.EstGagnant(nombreDeCasesDeJeu))
            {
                deSixFaces.BoutonDe.Enabled = false;
                MessageBox.Show("Le joueur " + (Rotation + 1) + " a gagné!");
                this.statistiques();
            }
            else
            {
                this.statistiques();
                this.passerAuProchainJoueur();
            }           
        }

        private bool DeterminerCasesEscabeauxSelon(int CaseDuJoueur)
        {
            bool CaseEscabeau = true;
            ++CaseDuJoueur;

            switch (CaseDuJoueur)
            {
                case 2: joueurActif.noCase = 42 - 1;
                    j.casesGagnees += (42-2);
                    j.nbEscabeaux += 1;
                    break;
                case 12: joueurActif.noCase = 27 - 1;
                    j.casesGagnees += (27-12);
                    j.nbEscabeaux += 1;
                    break;
                case 16: joueurActif.noCase = 50 - 1;
                    j.casesGagnees += (50-16);
                    j.nbEscabeaux += 1;
                    break;
                case 24: joueurActif.noCase = 37 - 1;
                    j.casesGagnees += (37-24);
                    j.nbEscabeaux += 1;
                    break;
                case 32: joueurActif.noCase = 67 - 1;
                    j.casesGagnees += (67-32);
                    j.nbEscabeaux += 1;
                    break;
                case 41: joueurActif.noCase = 83 - 1;
                    j.casesGagnees += (83-41);
                    j.nbEscabeaux += 1;
                    break;
                case 87: joueurActif.noCase = 94 - 1;
                    j.casesGagnees += (94-87);
                    j.nbEscabeaux += 1;
                    break;
                default: CaseEscabeau = false;
                    break;
            }

            return CaseEscabeau;
        }

        private bool DeterminerCasesCouleuvresSelon(int CaseDuJoueur)
        {
            bool CaseCouleuvre = true;
            ++CaseDuJoueur;

            switch (CaseDuJoueur)
            {
                case 15: joueurActif.noCase = 7 - 1;
                    j.casesPerdues += (15-7);
                    j.nbCouleuvres += 1;
                    break;
                case 38: joueurActif.noCase = 22 - 1;
                    j.casesPerdues += (38-22);
                    j.nbCouleuvres += 1;
                    break;
                case 53: joueurActif.noCase = 34 - 1;
                    j.casesPerdues += (53-34);
                    j.nbCouleuvres += 1;
                    break;
                case 68: joueurActif.noCase = 36 - 1;
                    j.casesPerdues += (68-36);
                    j.nbCouleuvres += 1;
                    break;
                case 72: joueurActif.noCase = 46 - 1;
                    j.casesPerdues += (72-46);
                    j.nbCouleuvres += 1;
                    break;
                case 98: joueurActif.noCase = 56 - 1;
                    j.casesPerdues += (98-56);
                    j.nbCouleuvres += 1;
                    break;
                default: CaseCouleuvre = false;
                    break;
            }

            return CaseCouleuvre;
        }

        public void statistiques()
        {
            tabJ[Rotation].casesGagnees += j.casesGagnees;
            tabJ[Rotation].casesPerdues += j.casesPerdues;
            tabJ[Rotation].nbEscabeaux += j.nbEscabeaux;
            tabJ[Rotation].nbCouleuvres += j.nbCouleuvres;
            j = new Joueur(0);
        }
    }
}
