﻿using System.IO;
using System.Windows.Forms;

namespace CouleuvresEtEscabeaux
{
    public partial class Statistiques : Form
    {
        //---------------------------------------------------------------------
        public Statistiques()
        {
            // Initialisation de la page Statistiques
            InitializeComponent();           
            read();
        }
        //---------------------------------------------------------------------
        private void read()
        {
            StreamReader sr = new StreamReader("Statistiques.txt");
            string line;
            Label[] tab = new Label[20];
            string nomLabel;

            // Ses boucles servent à mettre les labels de 6 à 25 dans un tableau.
            for (int i = 0; i <= 19; ++i)
            {
                nomLabel = "label" + (i+6);

                foreach (Control control in Controls)
                {
                    if (control.Name == nomLabel)
                    {
                        tab[i] = control as Label;
                        break;
                    }
                }
            }

            // Cette boucle sert a lire le document statistique.txt et afficher son contenu dans les labels 6 à 25.
            foreach (Label label in tab)
            {
                line = sr.ReadLine();
                label.Text = line;
            }

            for (int i = 1; i <= 4; i++)
            {
                chart1.Series["Joueur " + i].Points.AddXY(1, tab[(1 + (i - 1) + ((i - 1) * 4))].Text);
                chart1.Series["Joueur " + i].Points.AddXY(3, tab[(2 + (i - 1) + ((i - 1) * 4))].Text);
                chart2.Series["Joueur " + i].Points.AddXY(1, tab[(3 + (i - 1) + ((i - 1) * 4))].Text);
                chart2.Series["Joueur " + i].Points.AddXY(3, tab[(4 + (i - 1) + ((i - 1) * 4))].Text);
            }
            //close the file
            sr.Close();
        }
        //---------------------------------------------------------------------
    }
}
