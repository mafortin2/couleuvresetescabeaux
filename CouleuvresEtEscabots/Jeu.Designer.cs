﻿namespace CouleuvresEtEscabeaux
{
    partial class Jeu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Jeu));
            this.MenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.jeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvellePartieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.son = new System.Windows.Forms.ToolStripMenuItem();
            this.thèmesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.airToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistiquesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Player = new AxWMPLib.AxWindowsMediaPlayer();
            this.MenuPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuPrincipal
            // 
            this.MenuPrincipal.BackColor = System.Drawing.Color.Black;
            this.MenuPrincipal.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jeuToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.statistiquesToolStripMenuItem});
            this.MenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.MenuPrincipal.Name = "MenuPrincipal";
            this.MenuPrincipal.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.MenuPrincipal.Size = new System.Drawing.Size(1203, 28);
            this.MenuPrincipal.TabIndex = 0;
            this.MenuPrincipal.Text = "menuStrip1";
            // 
            // jeuToolStripMenuItem
            // 
            this.jeuToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.jeuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvellePartieToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.jeuToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.jeuToolStripMenuItem.Name = "jeuToolStripMenuItem";
            this.jeuToolStripMenuItem.Size = new System.Drawing.Size(42, 24);
            this.jeuToolStripMenuItem.Text = "&Jeu";
            // 
            // nouvellePartieToolStripMenuItem
            // 
            this.nouvellePartieToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.nouvellePartieToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.nouvellePartieToolStripMenuItem.Name = "nouvellePartieToolStripMenuItem";
            this.nouvellePartieToolStripMenuItem.Size = new System.Drawing.Size(179, 24);
            this.nouvellePartieToolStripMenuItem.Text = "&Nouvelle Partie";
            this.nouvellePartieToolStripMenuItem.Click += new System.EventHandler(this.nouvellePartieToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.quitterToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(179, 24);
            this.quitterToolStripMenuItem.Text = "&Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.son,
            this.thèmesToolStripMenuItem});
            this.optionsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // son
            // 
            this.son.BackColor = System.Drawing.Color.Black;
            this.son.ForeColor = System.Drawing.Color.White;
            this.son.Name = "son";
            this.son.Size = new System.Drawing.Size(175, 24);
            this.son.Text = "Son";
            this.son.Click += new System.EventHandler(this.son_Click);
            // 
            // thèmesToolStripMenuItem
            // 
            this.thèmesToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.thèmesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eauToolStripMenuItem,
            this.airToolStripMenuItem,
            this.terreToolStripMenuItem,
            this.feuToolStripMenuItem});
            this.thèmesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.thèmesToolStripMenuItem.Name = "thèmesToolStripMenuItem";
            this.thèmesToolStripMenuItem.Size = new System.Drawing.Size(175, 24);
            this.thèmesToolStripMenuItem.Text = "&Thèmes";
            // 
            // eauToolStripMenuItem
            // 
            this.eauToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.eauToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.eauToolStripMenuItem.Name = "eauToolStripMenuItem";
            this.eauToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.eauToolStripMenuItem.Text = "Eau";
            this.eauToolStripMenuItem.Click += new System.EventHandler(this.eauToolStripMenuItem_Click);
            // 
            // airToolStripMenuItem
            // 
            this.airToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.airToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.airToolStripMenuItem.Name = "airToolStripMenuItem";
            this.airToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.airToolStripMenuItem.Text = "Air";
            this.airToolStripMenuItem.Click += new System.EventHandler(this.airToolStripMenuItem_Click);
            // 
            // terreToolStripMenuItem
            // 
            this.terreToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.terreToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.terreToolStripMenuItem.Name = "terreToolStripMenuItem";
            this.terreToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.terreToolStripMenuItem.Text = "Terre";
            this.terreToolStripMenuItem.Click += new System.EventHandler(this.terreToolStripMenuItem_Click);
            // 
            // feuToolStripMenuItem
            // 
            this.feuToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.feuToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.feuToolStripMenuItem.Name = "feuToolStripMenuItem";
            this.feuToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.feuToolStripMenuItem.Text = "Feu";
            this.feuToolStripMenuItem.Click += new System.EventHandler(this.feuToolStripMenuItem_Click);
            // 
            // statistiquesToolStripMenuItem
            // 
            this.statistiquesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.statistiquesToolStripMenuItem.Name = "statistiquesToolStripMenuItem";
            this.statistiquesToolStripMenuItem.Size = new System.Drawing.Size(97, 24);
            this.statistiquesToolStripMenuItem.Text = "&Statistiques";
            this.statistiquesToolStripMenuItem.Click += new System.EventHandler(this.statistiquesToolStripMenuItem_Click);
            // 
            // Player
            // 
            this.Player.Enabled = true;
            this.Player.Location = new System.Drawing.Point(897, 635);
            this.Player.Name = "Player";
            this.Player.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Player.OcxState")));
            this.Player.Size = new System.Drawing.Size(262, 47);
            this.Player.TabIndex = 1;
            this.Player.Visible = false;
            // 
            // Jeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 882);
            this.Controls.Add(this.Player);
            this.Controls.Add(this.MenuPrincipal);
            this.MainMenuStrip = this.MenuPrincipal;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Jeu";
            this.Text = "Jeu";
            this.MenuPrincipal.ResumeLayout(false);
            this.MenuPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Player)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem jeuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvellePartieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistiquesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem son;
        private System.Windows.Forms.ToolStripMenuItem thèmesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem airToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feuToolStripMenuItem;
        private AxWMPLib.AxWindowsMediaPlayer Player;
    }
}