﻿using System.Drawing;
using System.Windows.Forms;

namespace CouleuvresEtEscabeaux
{
    class CaseDeJeu
    {
        public Button laCase = new Button();                        // Le carré qui apparait à l'écran
        private KnownColor couleurParDefaut = KnownColor.LightBlue; // Couleur de la case lorsqu'elle est inactive
        private int taille;                                         // Taille de la case
        public int CaseReelle = 0;
        private int noDeLaCase = 0;                                 // Numéro de la case
        private int noCaseDarrivee = 0;                             // Le numéro de la case où le joueur se retrouve s'il atterrit dessus

        public CaseDeJeu(int noDeLaCase, int taille, int NbCaseTotal)
        {
            this.noDeLaCase = noDeLaCase;
            this.taille = taille;
            this.CaseReelle = NbCaseTotal - noDeLaCase;
            this.configurerCaseDeJeu();

        }

        private void configurerCaseDeJeu()
        {
            Point position = determinerPosition();

            this.laCase.Name = "Case" + this.noDeLaCase;
            this.laCase.Font = new Font("Comic Sans MS", 10);
            this.laCase.Text = CaseReelle.ToString();
            this.laCase.Width = this.taille;
            this.laCase.Height = this.taille;
            this.laCase.Top = position.Y;
            this.laCase.Left = position.X;
            this.laCase.BackColor = determinerCouleurDeFond();
            DeterminerCouleuvreSelon(CaseReelle);
            DeterminerEscabeauSelon(CaseReelle);
            this.laCase.FlatStyle = FlatStyle.Flat;
            this.laCase.Enabled = false;
        }

        private void DeterminerCouleuvreSelon(int CaseCouleuvre)
        {
            Bitmap img;

            switch(CaseCouleuvre)
            {
                case 15: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_15_7");
                    laCase.BackgroundImage = img;
                    break;
                case 38: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_38_22");
                    laCase.BackgroundImage = img;
                    break;
                case 53: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_53_34");
                    laCase.BackgroundImage = img;
                    break;
                case 68: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_68_36");
                    laCase.BackgroundImage = img;
                    break;
                case 72: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_72_46");
                    laCase.BackgroundImage = img;
                    break;
                case 98: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_98_56");
                    laCase.BackgroundImage = img;
                    break;
                default: break;
            }

            switch (CaseCouleuvre)
            {
                case 7: laCase.BackColor = Color.Red;
                    break;
                case 22: laCase.BackColor = Color.DarkGoldenrod;
                    break;
                case 34: laCase.BackColor = Color.Green;
                    break;
                case 36: laCase.BackColor = Color.LightSeaGreen;
                    break;
                case 46: laCase.BackColor = Color.Orange;
                    break;
                case 56: laCase.BackColor = Color.DimGray;
                    break;
                default: break;
            }

        }

        private void DeterminerEscabeauSelon(int CaseEscabeau)
        {
            Bitmap img;

            switch (CaseEscabeau)
            {
                case 2: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_2_42");
                    laCase.BackgroundImage = img;
                    break;
                case 12: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_12_27");
                    laCase.BackgroundImage = img;
                    break;
                case 16: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_16_50");
                    laCase.BackgroundImage = img;
                    break;
                case 24: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_24_37");
                    laCase.BackgroundImage = img;
                    break;
                case 32: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_32_67");
                    laCase.BackgroundImage = img;
                    break;
                case 41: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_41_83");
                    laCase.BackgroundImage = img;
                    break;
                case 87: img = (Bitmap)Properties.Resources.ResourceManager.GetObject("_87_94");
                    laCase.BackgroundImage = img;
                    break;
                default: break;
            }

            switch (CaseEscabeau)
            {
                case 42: laCase.BackColor = Color.Gold;
                    break;
                case 27: laCase.BackColor = Color.MediumTurquoise;
                    break;
                case 50: laCase.BackColor = Color.Violet;
                    break;
                case 37: laCase.BackColor = Color.Purple;
                    break;
                case 67: laCase.BackColor = Color.Blue;
                    break;
                case 83: laCase.BackColor = Color.LightCoral;
                    break;
                case 94: laCase.BackColor = Color.Lime;
                    break;
                default: break;
            }
        }
        
        private Color determinerCouleurDeFond()
        {
            return Color.FromKnownColor(this.couleurParDefaut + 2 + (noDeLaCase % 3));
        }

        public Point determinerPosition()
        {
            int x;  // Abscices (Left)
            int y;  // Ordonnées (Top)

            if (this.noDeLaCase / 10 % 2 == 0)   // Rangées paires (gauche à droite)
            {
                x = this.taille + this.taille * (this.noDeLaCase % 10);
                y = this.taille + this.taille * (this.noDeLaCase / 10);
            }
            else                                // Rangées impaires (droite à gauche)
            {
                x = (this.taille * 10) - this.taille * (this.noDeLaCase % 10);
                y = this.taille + this.taille * (this.noDeLaCase / 10);
            }

            return new Point(x, y);
        }

        public Point obtenirPosition()
        {
            return new Point(this.laCase.Top, this.laCase.Left);
        }

        public void activer()
        {
            this.laCase.Enabled = true;
            this.laCase.FlatAppearance.BorderColor = Color.Gold;
            this.laCase.FlatAppearance.BorderSize = 4;
        }

        public void desactiver()
        {
            this.laCase.Enabled = false;
            this.laCase.FlatAppearance.BorderColor = Color.Black;
            this.laCase.FlatAppearance.BorderSize = 1;
        }
    }
}
