﻿using System.Drawing;
using System.Windows.Forms;

namespace CouleuvresEtEscabeaux
{
    class Joueur
    {
        public Button pion = new Button();  // Bouton représentant le joueur
        public int noDuJoueur = 0;         // Numéro du joueur: 0, 1, etc.
        private int taille = 30;            // Taille du joueur
        public int noCase = 0;             // Case du joueur par rapport à celle de départ
        public int casesGagnees = 0;       // Nombre de cases gagnées
        public int casesPerdues = 0;       // Nombre de cases perdues
        public int nbEscabeaux = 0;        // Nombre d'escabeaux (bonus)
        public int nbCouleuvres = 0;       // Nombre de couleuvres (malus)

        public Joueur(int noDuJoueur)
        {
            this.noDuJoueur = noDuJoueur;
            this.configurerPion();
        }

        public void deplacer(Point position)
        {
            Point nouvellePosition = this.ajusterPosition(position);

            this.pion.Top = nouvellePosition.X;
            this.pion.Left = nouvellePosition.Y;
        }

        public Point ajusterPosition(Point p)
        {
            switch (noDuJoueur) {
                case 1:
                    return new Point(p.X + 0, p.Y + this.taille);
                case 2:
                    return new Point(p.X + this.taille, p.Y + 0);
                case 3:
                    return new Point(p.X + this.taille, p.Y + this.taille);
                default: 
                    return new Point(p.X + 0, p.Y + 0);
            }
        }

        private void configurerPion()
        {
            this.pion.Name = "Joueur" + noDuJoueur;
            this.pion.Height = this.taille;
            this.pion.Width = this.taille;
            this.pion.BackgroundImage = determinerImageDeFond();
            this.pion.BackgroundImageLayout = ImageLayout.Stretch;
            this.pion.FlatStyle = FlatStyle.Flat;
            this.pion.Enabled = false;
        }
        
        private Bitmap determinerImageDeFond()
        {
            return (Bitmap)Properties.Resources.ResourceManager.GetObject("Pion_" + noDuJoueur);
        }

        // Indique si le joueur a dépassé la dernière case
        public bool EstTropLoin(int NbCasesTotal)
        {
            if (noCase > NbCasesTotal - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Indique si le joueur est tombé pile sur la dernière case
        public bool EstGagnant(int NbCasesTotal)
        {
            if (noCase == NbCasesTotal - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
