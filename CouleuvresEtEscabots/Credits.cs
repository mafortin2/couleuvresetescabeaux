﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CouleuvresEtEscabeaux
{
    public partial class Credits : Form
    {
        public Credits()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit(); // Le boutton "Quitter" ferme l'application.
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (label1.ForeColor != Color.Yellow)

                label1.ForeColor = Color.Yellow;
            else
                label1.ForeColor = Color.Green;
        }
    }
}
