﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using WMPLib;

namespace CouleuvresEtEscabeaux
{
    public partial class Jeu : Form
    {
        TableauDeJeu tableau;
        Joueur[] tab = new Joueur[4];
        Joueur j = new Joueur(0);
        DeSixFaces de = new DeSixFaces();

        public Jeu()
        {
            InitializeComponent();
            initialiserFenetre();
            initialiserControlesDeJeu();
            tableau.initialisertabJ();
            NouvellePartie();
            string chanson = Application.StartupPath.Substring(0, Application.StartupPath.LastIndexOf("\\bin\\"));  //Sa prend le path d'ou le fichier commence, sa enleve \\bin\\
            chanson += @"\Resources\song.mp3"; //On ajoute \Resouces\song.mp3    pour allez chercher la mouuusic !
            Player.URL = chanson; //Et on le donne au Window media player.
        }
        private void initialiserFenetre()
        {
            this.BackgroundImage = Properties.Resources.terre;
        }

        private void NouvellePartie()
        {
            StreamWriter sw = new StreamWriter("Statistiques.txt");
            sw.WriteLine(" ");
            sw.Close();
        }

        private void initialiserControlesDeJeu()
        {
            tableau = new TableauDeJeu(this);

            foreach (CaseDeJeu caseDeJeu in tableau.listeDesCasesDeJeu) {
                Controls.Add(caseDeJeu.laCase);
            }

            foreach (Joueur joueur in tableau.listeDesJoueurs) {
                Controls.Add(joueur.pion);
                joueur.pion.BringToFront();
            }

            Controls.Add(tableau.deSixFaces.BoutonDe);
        }

        public void StatistiquesEcrire()
        {
            StreamWriter sw = new StreamWriter("Statistiques.txt");
            for (int i = 0; i < 4;i++ )
            {
                sw.WriteLine("Joueur " + (i+1));
                sw.WriteLine(tableau.tabJ[i].casesGagnees);
                sw.WriteLine(tableau.tabJ[i].casesPerdues);
                sw.WriteLine(tableau.tabJ[i].nbEscabeaux);
                sw.WriteLine(tableau.tabJ[i].nbCouleuvres);
            }
            sw.Close();

        }
        private void nouvellePartieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void statistiquesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatistiquesEcrire();
            Statistiques stats = new Statistiques();
            stats.ShowDialog();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ouverture d'une nouvelle Windows Form pour les crédits.
            Credits credits = new Credits();
            credits.ShowDialog();
        }

        private void son_Click(object sender, EventArgs e)
        {
            if ((long)Player.playState == 3)
            {
                Player.Ctlcontrols.stop();
            }
            else
            {
                Player.Ctlcontrols.play();
            }
        }

        private void eauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Properties.Resources.eau;
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void airToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Properties.Resources.air;
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void terreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Properties.Resources.terre;
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void feuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackgroundImage = Properties.Resources.feu;
            this.BackgroundImageLayout = ImageLayout.Stretch;
        }
    }
}
